<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css">
    <title>Crea Usuari</title>
</head>
<body>
@section('content')
<h1>Crear Usuari</h1>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
        </ul>
    </div>
    @endif
@if(\Session::has('success'))
<div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
</div>
    @endif
<form action="CreaUsuari" method="post">
    {{csrf_field()}}
    @method('POST')
    <table>
        <tr>
            <td><label for="Nom">Nom:</label></td>
            <td><input type="text" name="nom" id="nom"></td>
        </tr>
        <tr>
            <td><label for="congnom">Cognom:</label></td>
            <td><input type="text" name="cognom" id="cognom"></td>
        </tr>
        <tr>
            <td><label for="nickname">Nickname:</label></td>
            <td><input type="text" name="nickname" id="nickname"></td>
        </tr>
    </table>
    <input type="submit" value="Enviar">
</form>
<script src="js/app.js" charset="utf-8"></script>
</body>
</html>