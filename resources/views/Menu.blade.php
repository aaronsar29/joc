<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css">
    <title>Manú</title>
</head>
<body>
<h1>Menú Principal</h1>
<div class="align-content-center">
<a href={{url('/CreaUser')}}>Crea Usuari</a>
<a href={{url('/RegistrarPuntuacio')}}>Registrar Puntuació</a>
<a href={{url('/Ranking')}}>Veure les Millors Puntuacions</a>
</div>
</body>
</html>