<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Init@show');

Route::post('CreaUsuari','Init@CreaUsuari');

Route::post('registrarPunts','Init@registrarPunts');

Route::get('/CreaUser', 'Init@CreaUser');

Route::get('/Ranking', 'Init@Ranking');

Route::get('Ranking', function () {


    $puntuacios = DB::table('puntuacios')
        -> orderBy('puntuacio', 'DESC')->take(20)-> get();
    return view('Ranking', [ 'puntuacios'=>$puntuacios]);
});

Route::get('/RegistrarPuntuacio', 'Init@registrarPuntuacio');



