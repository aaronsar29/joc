<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 04/04/2019
 * Time: 17:29
 */

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Puntuacio;
use App\puntuacion;
use App\Usuari;
use Illuminate\Http\Request;
use App\User;

class Init extends Controller
{

    public function show()
    {
        return view('Menu');
    }

    public function CreaUser(){
        return view('CreaUser');
    }

    public function Ranking(){
        return view('Ranking');
    }
    public function Success(){
        return view('Success');
    }

    public function registrarPuntuacio(){
        return view('RegistrarPuntuacio');
    }

    public function CreaUsuari(Request $req){
        $this->validate($req,[
            'nom' => 'required',
            'cognom' => 'required',
            'nickname' => 'required'
        ]);
        $usuari = new Usuari;
        $usuari->nom = $req->get('nom');
        $usuari->cognoms = $req->get('cognom');
        $usuari->nickname = $req->get('nickname');
        $usuari->save();

        return redirect('/') ->with('success' , 'Data Added');
    }

    public function registrarPunts(Request $req){
        $this->validate($req,[
            'nickname' => 'required',
            'puntuacio' => 'required'
        ]);
        $puntuacio = new Puntuacio;
        $puntuacio->nickname = $req->get('nickname');
        $puntuacio->puntuacio = $req->get('puntuacio');
        $puntuacio->save();

        return redirect('/') ->with('success' , 'Data Added');
    }


}